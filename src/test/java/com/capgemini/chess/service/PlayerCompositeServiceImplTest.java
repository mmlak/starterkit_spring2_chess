package com.capgemini.chess.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.exception.EntityNotFoundException;
import com.capgemini.chess.exception.InvalidInputDataException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.to.UserTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class PlayerCompositeServiceImplTest {

	@Autowired
	RankingService rankService;
	@Autowired
	UserService userService;
	@Autowired
	UserDao userDao;
	@Autowired
	UserCompositeService compService;

	@Test
	public void shouldReturnRanking() {
		// given

		// when
		List<UserTO> resultSet = compService.getRanking();

		for (UserTO u : resultSet) {
			System.out.println(u.getPoints());
		}
		// then
		assertEquals(2, resultSet.get(0).getPoints());
		assertEquals(1, resultSet.get(1).getPoints());
	}

	@Test
	public void shouldReadSingleUser() throws UserNotFoundException {
		// given
		int userId = 1;

		// when
		UserTO resultUser = compService.showUserProfile(userId);

		// then
		assertNotNull(resultUser);
		assertEquals(userId, resultUser.getId());
	}

	@Test
	public void shouldUpdatePlayerProfile() throws UserNotFoundException, EntityNotFoundException {
		// given
		UserTO user = new UserTO.UserTOBuilder().setId(1).setName("Ihaha").setSurname("surname").setEmail("NOWYMAIL")
				.setLogin("nowylogin").setPassword("nowehaslo").build();

		// when
		compService.updateProfile(user);

		// then
		assertEquals("Ihaha", userDao.findOne(1).getName());
	}

	@Test(expected = EntityNotFoundException.class)
	public void shouldNotUpdatePlayerProfileWithInvalidId() throws UserNotFoundException, EntityNotFoundException {
		// given
		int id = 33;
		UserTO user = new UserTO.UserTOBuilder().setId(id).setName("Ihaha").setSurname("surname").setEmail("NOWYMAIL")
				.setLogin("nowylogin").setPassword("nowehaslo").build();

		// when
		compService.updateProfile(user);

		// then
	}

	@Test(expected = UserNotFoundException.class)
	public void shouldThrowErrorIfInvalidId() throws UserNotFoundException {
		// given
		int userId = 33;

		// when
		compService.showUserProfile(userId);

		// then
		fail();
	}

	@Test
	public void shouldAddUser() throws InvalidInputDataException {
		// given
		UserTO user = new UserTO.UserTOBuilder().setEmail("asdasdadsd").setName("mi").setSurname("da").setLogin("log")
				.setPassword("pas").setPoints(1).build();

		// when
		compService.addUser(user);

		// then
		assertEquals(3, compService.findAll().size());

	}
	
	@Test
	public void shouldNotAddUserWithIncompleteData() throws InvalidInputDataException{
		UserTO user = new UserTO.UserTOBuilder().setName("mi").setSurname("da").setLogin("log")
				.setPassword("pas").setPoints(1).build();

		// when
		compService.addUser(user);

		// then
		assertEquals(3, compService.findAll().size());
	}

}
