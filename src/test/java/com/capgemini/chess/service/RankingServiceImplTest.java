package com.capgemini.chess.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.chess.dao.MatchDao;
import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.UserTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class RankingServiceImplTest {
	@Autowired
	RankingService rs;

	@Autowired
	UserDao userDao;

	@Autowired
	MatchDao matchDao;

	@Test
	public void shouldReturnRanking() {
		// given

		// when
		List<UserTO> resultSet = new ArrayList<>();
		resultSet.addAll(Mapper.mapUser2TO(rs.getRanking()));

		// then
		assertEquals(2, resultSet.get(0).getPoints());
		assertEquals(1, resultSet.get(1).getPoints());
	}

	@Test
	public void shouldGetPointsForPlayer() throws UserNotFoundException {
		// given
		int userId = 1;
		// when
		int result = rs.getPointsForUser(userId);

		// then
		assertEquals(2, result);

	}

	@Test
	public void shouldReturnUserRankingPosition() {
		// given
		UserTO testUser = new UserTO.UserTOBuilder().setId(1).setPoints(23).build();

		// when
		int rankingPos = rs.getUserPositionInRanking(testUser);

		// then
		assertEquals(1, rankingPos);
	}

}
