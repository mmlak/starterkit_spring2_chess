package com.capgemini.chess.service;

import static org.junit.Assert.assertNotNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.UserTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class UserServiceTest {

	@Autowired
	UserService service;

	@Test
	public void testReadUserSuccessful() throws Exception {

		// when
		UserTO userTO = Mapper.map(service.readUser(1));
		assertNotNull(userTO);
	}

}
