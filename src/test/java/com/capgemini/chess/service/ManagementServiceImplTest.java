package com.capgemini.chess.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.chess.dao.MatchDao;
import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.enums.GameState;
import com.capgemini.chess.exception.InvalidMatchFormatException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ManagementServiceImplTest {

	@Autowired
	MatchDao matchDao;

	@Autowired
	UserDao userDao;

	@Autowired
	ManagementService ms;

	@Test
	public void shouldUpdatePlayerPoints() throws UserNotFoundException {
		// given
		UserTO loser = new UserTO.UserTOBuilder().setLogin("a").setPassword("a").setId(2).setName("name").setSurname("sr")
				.setEmail("em").build();
		UserTO winner = new UserTO.UserTOBuilder().setLogin("ww").setId(1).setPassword("pw").setName("a")
				.setSurname("s").setEmail("e").build();

		MatchTO matchTwo = new MatchTO.MatchTOBuilder().setGameState(GameState.FINISHED).setLastMovePlayer(loser)
				.setLoser(loser).setLoserPoints(1).setTimeSinceLastMove(1).setWinner(winner).setWinnerPoints(3).build();

		ms.addNewMatch(matchTwo);
		// userDao.addMatchToUsers(Mapper.map(match));

		// when
		ms.updatePlayerPoints();

		// then
		assertEquals(2, matchDao.findAll().size());
		assertEquals(1, matchDao.findOne(2).getWinner().getId());
		assertEquals(5, userDao.findOne(1).getPoints());

	}

	@Test
	public void shouldUpdatePlayersMatchHistory() throws InvalidMatchFormatException, UserNotFoundException {
		// given
		UserTO loser = new UserTO.UserTOBuilder().setLogin("a").setId(2).setPassword("a").setName("name").setSurname("sr")
				.setEmail("em").build();
		UserTO winner = new UserTO.UserTOBuilder().setLogin("ww").setId(1).setPassword("pw").setName("a")
				.setSurname("s").setEmail("e").build();

		MatchTO matchOne = new MatchTO.MatchTOBuilder().setWinner(winner).setLoser(loser).setLoserPoints(1)
				.setWinnerPoints(1).setLastMovePlayer(winner).setTimeSinceLastMove(3)
				.setGameState(GameState.NOT_FINISHED).build();

		// when
		ms.addNewMatch(matchOne);
		ms.updatePlayersMatchHistory();

		// then
		assertEquals(2, userDao.findOne(1).getMatchSet().size());
	}

	@Test
	public void shouldAddNewMatch() {
		// given
		UserTO user67 = Mapper.map(userDao.findOne(1));
		UserTO user30 = Mapper.map(userDao.findOne(2));
		MatchTO testMatch = new MatchTO.MatchTOBuilder().setWinner(user67).setLoser(user30).setWinnerPoints(3)
				.setLoserPoints(-5).setLastMovePlayer(user67).setTimeSinceLastMove(333).setGameState(GameState.FINISHED)
				.build();
		ms.addNewMatch(testMatch);

		// when
		List<MatchBO> resultMatch = matchDao.findAll();

		assertEquals(2, resultMatch.size());
	}
}
