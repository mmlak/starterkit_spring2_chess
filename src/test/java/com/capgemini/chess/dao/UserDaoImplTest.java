package com.capgemini.chess.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.UserTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
public class UserDaoImplTest {

	@Autowired
	UserDao dao;

	@Test
	public void shoudlReturnOneUser() throws UserNotFoundException {
		assertNotNull(dao.findOne(1));
	}

	@Test
	public void shouldReturnNullIfNoSuchUser() throws UserNotFoundException {
		// given

		int id = 16;

		// when

		UserBO user = dao.findOne(id);

		// then
		assertEquals(null, user);
	}

	@Test
	public void shouldGetAllUsers() {
		// given

		// when
		Set<UserTO> resultSet = new HashSet<>();
		resultSet.addAll(Mapper.mapUser2TO(dao.findAll()));

		// then
		assertEquals(2, resultSet.size());
	}

	@Test
	public void shouldUpdateUserProfile() throws UserNotFoundException {
		// given
		Set<UserTO> expectedSet = new HashSet<>();
		expectedSet
				.add(new UserTO.UserTOBuilder().setId(1).setName("Michal").setSurname("Kowalski").setPassword("12f23")
						.setSurname("admin1").setEmail("ASdaw@daw4.pl").setPoints(2).setLogin("login2").build());
		UserTO updatedUser = new UserTO.UserTOBuilder().setId(1).setName("Marcin").setSurname("Krakowski")
				.setPassword("Haslo123").setSurname("Nazwisko").setEmail("ASdaw@niema.pl").setPoints(2)
				.setLogin("login1").build();
		// when
		dao.update(Mapper.map(updatedUser));

		// then
		assertEquals("Haslo123", dao.findOne(1).getPassword());
		assertEquals("Nazwisko", dao.findOne(1).getSurname());

	}

	@Test
	public void shouldAddUser() {
		// given
		UserBO user67 = new UserBO.UserEntityBuilder().setLogin("aaa").setPassword("passwd").setName("name")
				.setSurname("surname").setEmail("email@dom.org").build();

		dao.save(user67);
		// when

	}

}
