package com.capgemini.chess.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.enums.GameState;
import com.capgemini.chess.exception.InvalidMatchFormatException;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserTO;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class MatchDaoImplTest {

	@Autowired
	MatchDao dao;

	@Test
	public void shouldReturnAllMatchesForUser() {
		// given
		int userId = 1;

		// when
		int matchCount = dao.getAllMatchesForUser(userId).size();

		// then
		assertEquals(1, matchCount);
	}

	@Test
	public void shouldCreateNewMatch() throws InvalidMatchFormatException {
		// given
		UserTO user67 = new UserTO.UserTOBuilder().setId(67).setLogin("aaa").setPassword("passwd").setName("name")
				.setSurname("surname").setEmail("email@dom.org").build();
		UserTO user30 = new UserTO.UserTOBuilder().setId(30).setLogin("bbb").setPassword("pwd").setName("name2")
				.setSurname("surname2").setEmail("email2@dom2.org").build();
		MatchTO testMatch = new MatchTO.MatchTOBuilder().setWinner(user67).setLoser(user30).setWinnerPoints(3)
				.setLoserPoints(-5).setMatchId(305).setLastMovePlayer(user67).setGameState(GameState.FINISHED).build();
		MatchBO mecz = new MatchBO.MatchBOBuilder().setId(13).setLastMovePlayer(Mapper.map(user30))
				.setLoser(Mapper.map(user30)).setLoserPoints(0).setTimeSinceLastMove(123).setWinner(Mapper.map(user67))
				.setWinnerPoints(3).build();
		dao.save(mecz);

		// when
		List<MatchBO> resultMatch = dao.getAllMatchesForUser(67);

		assertEquals(testMatch, resultMatch.toArray()[0]);
	}

	@Test(expected = InvalidMatchFormatException.class)
	public void shouldNotCreateMatchWithSelf() throws InvalidMatchFormatException {
		// given
		UserTO user30 = new UserTO.UserTOBuilder().setId(30).build();
		MatchTO testMatch = new MatchTO.MatchTOBuilder().setWinner(user30).setLoser(user30).setWinnerPoints(1)
				.setLoserPoints(1).build();
		// when
		dao.save(Mapper.map(testMatch));

		// then
		fail();

	}

	@Test
	public void shouldReturnAllMatches() {
		// given

		// when
		Set<MatchTO> resultSet = new HashSet<>();
		resultSet.addAll(Mapper.mapMatch2TO(dao.findAll()));

		// then
		assertEquals(1, resultSet.size());
	}

	@Test(expected = InvalidMatchFormatException.class)
	public void shouldNotAddMatchIfWinnerAndLoserIsEqual() throws InvalidMatchFormatException {
		// given
		UserTO winner = new UserTO.UserTOBuilder().setId(1).setName("a").setSurname("b").setLogin("asd")
				.setPassword("asdas").setEmail("dawwr").build();
		UserTO loser = new UserTO.UserTOBuilder().setId(2).setName("s").setSurname("ss").setLogin("dsa")
				.setPassword("daadsa").setEmail("aaaaa").build();
		MatchTO match = new MatchTO.MatchTOBuilder().setWinner(winner).setLoser(loser).setMatchId(13)
				.setLastMovePlayer(winner).setTimeSinceLastMove(3123).build();

		// when
		MatchBO mecz = Mapper.map(match);
		dao.save(mecz);

		// then
		fail();
	}

}
