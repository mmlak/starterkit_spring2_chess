//package com.capgemini.chess.rest;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.io.File;
//import java.util.ArrayList;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultActions;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import com.capgemini.chess.service.UserCompositeService;
//import com.capgemini.chess.service.to.UserTO;
//import com.capgemini.chess.utils.FileUtils;
//
//public class UserCompositeRESTServiceTest {
//
//	@Autowired
//	private UserCompositeRESTService userCompositeRestService;
//
//	@Autowired
//	private UserCompositeService userCompositeService;
//
//	@Test
//	public void testShouldShowUserProfile() throws Exception {
//
//		// given:
//		final UserTO userTo1 = new UserTO.UserTOBuilder().setId(1).setName("Daria").setSurname("Nowak")
//				.setLogin("login").setPassword("password").build();
//
//		// when
//		ResultActions response = this.mockMvc
//				.perform(get("/user/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON));
//
//		response.andExpect(status().isOk()).andExpect(jsonPath("$.id").value((int) userTo1.getId()))
//				.andExpect(jsonPath("$.password").value(userTo1.getPassword()))
//				.andExpect(jsonPath("$.login").value(userTo1.getLogin()));
//	}
//
//	@Test
//	public void shouldGetRanking() throws Exception {
//		// given
//		final UserTO userTo1 = new UserTO.UserTOBuilder().setId(1L).setName("Daria").setSurname("Nowak")
//				.setLogin("login").setPassword("password").setPoints(30).build();
//		final UserTO userTo2 = new UserTO.UserTOBuilder().setId(2L).setName("Dariusz").setSurname("Kowalski")
//				.setLogin("loginek").setPassword("haselko").setPoints(1).build();
//		ArrayList<UserTO> userList = new ArrayList<>();
//		userList.add(userTo1);
//		userList.add(userTo2);
//		Mockito.when(userCompositeService.getRanking()).thenReturn(userList);
//
//		// when
//		ResultActions response = mockMvc
//				.perform(get("/ranking").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON));
//
//		// then
//		response.andExpect(status().isOk()).andExpect(jsonPath("[0].id").value((int) userTo1.getId()))
//				.andExpect(jsonPath("[1].id").value((int) userTo2.getId()));
//
//	}
//
//	@Test
//	public void shouldUpdateUserProfile() throws Exception {
//		// given
//		File file = FileUtils.getFileFromClasspath("classpath:/com/capgemini/chess/profileToUpdate.json");
//		String json = FileUtils.readFileToString(file);
//
//		// when
//		ResultActions response = this.mockMvc.perform(put("/user/update").accept(MediaType.APPLICATION_JSON)
//				.contentType(MediaType.APPLICATION_JSON).content(json.getBytes()));
//
//		// then
//		response.andExpect(status().isOk());
//	}
//
//}
