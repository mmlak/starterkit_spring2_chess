INSERT into users (id, login, password, name, surname, email, points) values (1, 'misza', 'haslo123', 'michal', 'gewert', 'mail@a.pl', 2);
INSERT into users (id, login, password, name, surname, email, points) values (2, 'marek', 'niemamhasla', 'marek', 'skoczylas', 'dzimejl@dzimejl.com', 1);


insert into matches (id, winner, loser, time_since_last_move, last_moved_player, game_state) values (1, 1, 2, 12314,1,'FINISHED');

insert into users_match_set ( userbo_id, match_set_id) values (1, 1);
insert into users_match_set ( userbo_id, match_set_id) values (2, 1);

insert into tasks (id, title, category, task, date,priority) values (1, 'First task', 'SHOPPING', 'I should buy a boat', '1992-01-01', 'HIGH');
insert into tasks (id, title, category, task, date,priority) values (2, 'Should visit grandma', 'REMINDER', 'Akacjowa 3/5', '1992-01-01', 'NORMAL');
insert into tasks (id, title, category, task, date,priority) values (3, 'Job recruitment', 'MEETING', 'At capgemini mt2', '2016-08-30','LOW');
insert into tasks (id, title, category, task, date,priority) values (4, 'Suit', 'SHOPPING', 'buy a suit for wedding', '2016-11-11','HIGH');
insert into tasks (id, title, category, task, date,priority) values (5, 'engagement ring', 'SHOPPING', 'Its high time mike!', '2020-01-01','NORMAL');
insert into tasks (id, title, category, task, date,priority) values (6, 'password', 'REMINDER', 'Xfas23!da', '1992-01-01', 'LOW');
insert into tasks (id, title, category, task, date,priority) values (7, 'Xmas!', 'REMINDER', 'Dont forget to get back home for xmas!', '2016-12-26', 'HIGH');
insert into tasks (id, title, category, task, date,priority) values (8, 'new years eve', 'REMINDER', 'this year at 13.12 on friday!', '2016-12-13', 'HIGH');
insert into tasks (id, title, category, task, date,priority) values (9, 'santa meeting', 'MEETING', 'This year santa is really coming!!!', '2016-12-06', 'LOW');
insert into tasks (id, title, category, task, date,priority) values (10, 'PYRKON!!', 'REMINDER', 'POZNAN!', '2017-04-28', 'NORMAL');
insert into tasks (id, title, category, task, date,priority) values (11, 'pink slippers', 'SHOPPING', 'guilty pleasure', '1992-01-01', 'LOW');
insert into tasks (id, title, category, task, date,priority) values (12, 'burger', 'SHOPPING', 'pasibus ofc', '2019-02-05','NORMAL');
insert into tasks (id, title, category, task, date,priority) values (13, 'koniec startera', 'REMINDER', 'to juz koniec', '2016-12-16','NORMAL');
insert into tasks (id, title, category, task, date,priority) values (14, 'nowy rok', 'REMINDER', 'jak zawsze 13 w piatek', '2016-12-31','NORMAL');
