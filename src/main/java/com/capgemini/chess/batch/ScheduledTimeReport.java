package com.capgemini.chess.batch;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.capgemini.chess.exception.InvalidMatchFormatException;
import com.capgemini.chess.service.ManagementService;

@Component
public class ScheduledTimeReport {

	@Autowired
	ManagementService ms;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() throws InvalidMatchFormatException {

//		System.out.println("Completed matches: " + ms.arbitrarlyEndMatchesWithTooLongNoAction());
		System.out.println("The time is now " + dateFormat.format(new Date()));
	}
}
