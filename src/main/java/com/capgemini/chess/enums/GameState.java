package com.capgemini.chess.enums;

public enum GameState {
	FINISHED, NOT_FINISHED
}
