package com.capgemini.chess.enums;

public enum TaskStatus {

	NOT_FINISHED, FINISHED, CANCELED, SUSPENDED;
}
