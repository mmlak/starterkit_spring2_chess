package com.capgemini.chess.dao;

import com.capgemini.chess.dataaccess.entities.UserBO;

/**
 * Interface used for connecting user database.
 * 
 * @author MIMLAK
 *
 */
public interface UserDao extends Dao<UserBO, Integer> {

}
