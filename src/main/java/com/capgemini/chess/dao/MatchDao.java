package com.capgemini.chess.dao;

import java.util.List;

import com.capgemini.chess.dataaccess.entities.MatchBO;

/**
 * Connects to match database.
 * 
 * @author MIMLAK
 *
 */
public interface MatchDao extends Dao<MatchBO, Integer> {

	/**
	 * Returns a set of matches played by given player
	 * 
	 * @param id
	 *            finds an user by id
	 * @return Set of matches played
	 */
	public List<MatchBO> getAllMatchesForUser(int id);

}
