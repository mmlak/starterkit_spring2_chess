package com.capgemini.chess.dao.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dao.TaskDao;
import com.capgemini.chess.dataaccess.entities.TaskEntity;

@Repository
@Transactional
public class TaskDaoImpl extends AbstractDao <TaskEntity, Integer> implements TaskDao {

}
