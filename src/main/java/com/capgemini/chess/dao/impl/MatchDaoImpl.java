package com.capgemini.chess.dao.impl;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dao.MatchDao;
import com.capgemini.chess.dataaccess.entities.MatchBO;

@Repository
@Transactional
public class MatchDaoImpl extends AbstractDao<MatchBO, Integer> implements MatchDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<MatchBO> getAllMatchesForUser(int id) {
		Query query = entityManager.createNamedQuery("getAllMatchesForUser");
		query.setParameter("userId", id);
		List<MatchBO> resultList = query.getResultList();
		return resultList;
	}

}
