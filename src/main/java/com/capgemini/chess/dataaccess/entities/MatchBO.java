package com.capgemini.chess.dataaccess.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.capgemini.chess.enums.GameState;

@Entity
@NamedQuery(name = "getAllMatchesForUser", query = "select m from MatchBO m where m.loser.id = :userId or m.winner.id = :userId")
@Table(name = "matches")

public class MatchBO extends AbstractEntity {

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id = 0;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "loser")
	private UserBO loser;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "winner")
	private UserBO winner;
	@Column(name = "time_since_last_move", nullable = false)
	private long timeSinceLastMove;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "last_moved_player", nullable = false)
	private UserBO lastMovePlayer;
	@Enumerated(EnumType.STRING)
	@Column(name = "game_state", nullable = false, columnDefinition = "ENUM('FINISHED', 'NOT_FINISHED')")
	private GameState gameState;
	@Column(name = "winner_points", columnDefinition = "int default 0")
	private int winnerPoints;
	@Column(name = "loser_points", columnDefinition = "int default 0")
	private int loserPoints;
	private CoreData coreData;

	public CoreData getCoreData() {
		return coreData;
	}

	public void setCoreData(CoreData coreData) {
		this.coreData = coreData;
	}

	public void setLoserPoints(int loserPoints) {
		this.loserPoints = loserPoints;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public long getTimeSinceLastMove() {
		return timeSinceLastMove;
	}

	public void setTimeSinceLastMove(long timeSinceLastMove) {
		this.timeSinceLastMove = timeSinceLastMove;
	}

	public UserBO getLastMovePlayer() {
		return lastMovePlayer;
	}

	public void setLastMovePlayer(UserBO lastMovePlayer) {
		this.lastMovePlayer = lastMovePlayer;
	}

	public int getWinnerPoints() {
		return winnerPoints;
	}

	public void setWinnerPoints(int winnerPoints) {
		this.winnerPoints = winnerPoints;
	}

	public int getLoserPoints() {
		return loserPoints;
	}

	public void setLoserPoitns(int loserPoitns) {
		this.loserPoints = loserPoitns;
	}

	@Override
	public String toString() {
		return "MatchBO [" + "playerTwoId=" + loser + ", winnerId=" + winner + "]";
	}

	public UserBO getLoser() {
		return loser;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gameState == null) ? 0 : gameState.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((lastMovePlayer == null) ? 0 : lastMovePlayer.hashCode());
		result = prime * result + ((loser == null) ? 0 : loser.hashCode());
		result = prime * result + loserPoints;
		result = prime * result + (int) (timeSinceLastMove ^ (timeSinceLastMove >>> 32));
		result = prime * result + ((winner == null) ? 0 : winner.hashCode());
		result = prime * result + winnerPoints;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchBO other = (MatchBO) obj;
		if (gameState != other.gameState)
			return false;
		if (id != other.id)
			return false;
		if (lastMovePlayer == null) {
			if (other.lastMovePlayer != null)
				return false;
		} else if (!lastMovePlayer.equals(other.lastMovePlayer))
			return false;
		if (loser == null) {
			if (other.loser != null)
				return false;
		} else if (!loser.equals(other.loser))
			return false;
		if (loserPoints != other.loserPoints)
			return false;
		if (timeSinceLastMove != other.timeSinceLastMove)
			return false;
		if (winner == null) {
			if (other.winner != null)
				return false;
		} else if (!winner.equals(other.winner))
			return false;
		if (winnerPoints != other.winnerPoints)
			return false;
		return true;
	}

	public void setLoser(UserBO loser) {
		this.loser = loser;
	}

	public UserBO getWinner() {
		return winner;
	}

	public void setWinner(UserBO winner) {
		this.winner = winner;
	}

	public MatchBO() {

	}

	public MatchBO(MatchBOBuilder builder) {
		this.loser = builder.loser;
		this.winner = builder.winner;
		this.winnerPoints = builder.winnerPoints;
		this.loserPoints = builder.loserPoints;
		this.timeSinceLastMove = builder.timeSinceLastMove;
		this.lastMovePlayer = builder.lastMovePlayer;
		this.gameState = builder.gameState;
		this.id = builder.id;
	}

	public static class MatchBOBuilder {

		private GameState gameState;
		private long timeSinceLastMove;
		private UserBO lastMovePlayer;
		private UserBO loser;
		private UserBO winner;
		private int winnerPoints;
		private int loserPoints;
		private int id;

		public MatchBOBuilder setGameState(GameState state) {
			this.gameState = state;
			return this;
		}

		public MatchBOBuilder setTimeSinceLastMove(long time) {
			this.timeSinceLastMove = time;
			return this;
		}

		public MatchBOBuilder setLastMovePlayer(UserBO user) {
			this.lastMovePlayer = user;
			return this;
		}

		public MatchBOBuilder setId(int id) {
			this.id = id;
			return this;
		}

		public MatchBOBuilder setLoserPoints(int points) {
			this.loserPoints = points;
			return this;
		}

		public MatchBOBuilder setWinnerPoints(int points) {
			this.winnerPoints = points;
			return this;
		}

		public MatchBOBuilder setLoser(UserBO loser) {
			this.loser = loser;
			return this;
		}

		public MatchBOBuilder setWinner(UserBO winner) {
			this.winner = winner;
			return this;
		}

		public MatchBO build() {
			return new MatchBO(this);
		}
	}
}
