package com.capgemini.chess.dataaccess.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CoreData {
	@Column(name = "created", columnDefinition = "TIMESTAMP default current_timestamp")
	private Date createdAt;
	@Column(name = "last_modified_at", columnDefinition = "TIMESTAMP DEFAULT current_timestamp ON UPDATE current_timestamp")
	private Date lastModifiedAt;

	
	public CoreData(){
		
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}

	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}



	
}
