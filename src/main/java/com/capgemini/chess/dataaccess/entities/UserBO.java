package com.capgemini.chess.dataaccess.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UserBO extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "login", nullable = false)
	private String login;
	@Column(name = "password", nullable = false)
	private String password;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "surname", nullable = false)
	private String surname;
	@Column(name = "email", nullable = false)
	private String email;
	@Column(name = "about_me")
	private String aboutMe;
	@Column(name = "life_motto")
	private String lifeMotto;
	@Column(name = "points", columnDefinition = "int default 0")
	private int points = 0;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "match_list")
	private Set<MatchBO> matchSet = new HashSet<>();
	private CoreData coreData;

	public CoreData getCoreData() {
		return coreData;
	}

	public void setCoreData(CoreData coreData) {
		this.coreData = coreData;
	}

	public void setMatchSet(Set<MatchBO> matchSet) {
		this.matchSet = matchSet;
	}

	public int getPoints() {
		return points;
	}

	public void addPoints(int points) {
		this.points += points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Set<MatchBO> getMatchSet() {
		return matchSet;
	}

	public void addMatch(MatchBO match) {
		this.matchSet.add(match);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getLifeMotto() {
		return lifeMotto;
	}

	public void setLifeMotto(String lifeMotto) {
		this.lifeMotto = lifeMotto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "UserBO [id=" + id + ", login=" + login + ", password=" + password + ", name=" + name + ", surname="
				+ surname + ", points=" + points + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserBO other = (UserBO) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public UserBO() {

	}

	public UserBO(UserEntityBuilder builder) {
		this.id = builder.id;
		this.login = builder.login;
		this.password = builder.password;
		this.name = builder.firstName;
		this.surname = builder.surname;
		this.email = builder.eMail;
		this.aboutMe = builder.aboutMe;
		this.lifeMotto = builder.lifeMotto;
		this.points = builder.points;
		this.matchSet = builder.matchSet;
	}

	public static class UserEntityBuilder {
		private int id;
		private String login;
		private String password;
		private String firstName;
		private String surname;
		private String lifeMotto;
		private String aboutMe;
		private String eMail;
		private int points;
		private Set<MatchBO> matchSet = new HashSet<>();

		public UserEntityBuilder setPoints(int points) {
			this.points = points;
			return this;
		}

		public UserEntityBuilder setMatch(Set<MatchBO> matchSet) {
			this.matchSet = matchSet;
			return this;
		}

		public UserEntityBuilder setId(int id) {
			this.id = id;
			return this;
		}

		public UserEntityBuilder setLogin(String login) {
			this.login = login;
			return this;
		}

		public UserEntityBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public UserEntityBuilder setName(String name) {
			this.firstName = name;
			return this;
		}

		public UserEntityBuilder setSurname(String surname) {
			this.surname = surname;
			return this;
		}

		public UserEntityBuilder setLifeMotto(String motto) {
			this.lifeMotto = motto;
			return this;
		}

		public UserEntityBuilder setAboutMe(String aboutMe) {
			this.aboutMe = aboutMe;
			return this;
		}

		public UserEntityBuilder setEmail(String eMail) {
			this.eMail = eMail;
			return this;
		}

		public UserBO build() {
			return new UserBO(this);
		}

	}

}
