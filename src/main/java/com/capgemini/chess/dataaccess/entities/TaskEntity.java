package com.capgemini.chess.dataaccess.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.capgemini.chess.enums.TaskCategory;
import com.capgemini.chess.enums.TaskPriority;
import com.capgemini.chess.enums.TaskStatus;

@Entity
@Table(name="tasks")
public class TaskEntity extends AbstractEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name="title")
	private String title;
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private TaskCategory category;
	@Lob
	@Column(name="task")
	private String task;
	@Column(name="date")
	private Date date;
	@Enumerated(EnumType.STRING)
	@Column(name="priority")
	private TaskPriority taskPriority;
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private TaskStatus status;
	private CoreData coreData;
	
	
	public TaskEntity(){
		
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public TaskCategory getCategory() {
		return category;
	}


	public void setCategory(TaskCategory category) {
		this.category = category;
	}


	public String getTask() {
		return task;
	}


	public void setTask(String task) {
		this.task = task;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public CoreData getCoreData() {
		return coreData;
	}


	public void setCoreData(CoreData coreData) {
		this.coreData = coreData;
	}


	public TaskPriority getTaskPriority() {
		return taskPriority;
	}


	public void setTaskPriority(TaskPriority taskPriority) {
		this.taskPriority = taskPriority;
	}


	

}
