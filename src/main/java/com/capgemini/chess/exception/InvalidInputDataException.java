package com.capgemini.chess.exception;

public class InvalidInputDataException extends Exception {

	public InvalidInputDataException() {

	}

	public InvalidInputDataException(Object o) {
		super("Invalid input data for entity: " + o.getClass());
	}

}
