package com.capgemini.chess.exception;

public class EntityNotFoundException extends Exception {

	public EntityNotFoundException() {

	}

	public EntityNotFoundException(Object o) {
		super("Entity " + o.getClass() + "not found!");
	}

}
