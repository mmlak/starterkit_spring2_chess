package com.capgemini.chess.exception;

public class InvalidMatchFormatException extends Exception {

	public InvalidMatchFormatException() {

	}

	public InvalidMatchFormatException(String message) {
		super(message);
	}
}
