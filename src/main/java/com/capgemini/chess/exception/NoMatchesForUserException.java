package com.capgemini.chess.exception;

public class NoMatchesForUserException extends Exception {
	public NoMatchesForUserException() {

	}

	public NoMatchesForUserException(String message) {
		super(message);
	}

}
