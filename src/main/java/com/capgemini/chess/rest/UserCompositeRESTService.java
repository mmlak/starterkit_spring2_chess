package com.capgemini.chess.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.chess.exception.EntityNotFoundException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.UserCompositeService;
import com.capgemini.chess.service.to.UserTO;

/**
 * Rest service for user composite service
 * 
 * @author MIMLAK
 *
 */
@Controller
@ResponseBody
public class UserCompositeRESTService {
	@Autowired
	UserCompositeService userCompositeService;

	/**
	 * Finds and returns user profile
	 * 
	 * @param id
	 *            user's id
	 * @return user profile as UserTO
	 * @throws UserNotFoundException
	 */
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserTO getUserProfile(@PathVariable("id") int id) throws UserNotFoundException {
		return userCompositeService.showUserProfile(id);
	}

	/**
	 * Returns user's ranking by points
	 * 
	 * @return user's ranking as List<TO>
	 */
	@RequestMapping(value = "/ranking", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserTO> getRanking() {
		return userCompositeService.getRanking();
	}

	/**
	 * Updates user profile
	 * 
	 * @param user
	 * @return
	 * @throws UserNotFoundException
	 * @throws EntityNotFoundException 
	 */
	@RequestMapping(value = "/user/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public UserTO updateProfile(@RequestBody UserTO user) throws UserNotFoundException, EntityNotFoundException {
		userCompositeService.updateProfile(user);
		return userCompositeService.showUserProfile(user.getId());
	}

}
