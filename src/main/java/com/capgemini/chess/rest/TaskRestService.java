package com.capgemini.chess.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.chess.dataaccess.entities.TaskEntity;
import com.capgemini.chess.exception.EntityNotFoundException;
import com.capgemini.chess.service.TaskService;

class TaskComparator implements Comparator<TaskEntity>{

	@Override
	public int compare(TaskEntity o1, TaskEntity o2) {
		if(o1.getId() > o2.getId()){
			return -1;
		}
		else if(o2.getId() > o1.getId()){
			return 1;
		}
		return 0;
	}
	
}

@Controller
@ResponseBody
@Transactional
public class TaskRestService {
	
	@Autowired
	TaskService taskService;
	
//	@CrossOrigin(origins="http://localhost:9000/")
	@CrossOrigin
	@RequestMapping(value="/rest/tasks/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TaskEntity> getAllTasks(){
		List<TaskEntity> tempList = new ArrayList<>();
		tempList = taskService.findAll();
		Collections.sort(tempList, new TaskComparator());
		return tempList;
	}
	
	@CrossOrigin
	@RequestMapping(value="/rest/tasks/task/{id}", method = RequestMethod.DELETE)
	public void deleteTask(@PathVariable("id") int id){
		taskService.delete(id);
	}
	
	@CrossOrigin
	@RequestMapping(value="/rest/tasks/task", method = RequestMethod.POST)
	public void addTask(@RequestBody TaskEntity task){
		taskService.add(task);
	}
	
	@CrossOrigin
	@RequestMapping(value="/rest/tasks/task", method = RequestMethod.PUT)
	public void updateTask(@RequestBody TaskEntity task) throws EntityNotFoundException{
		TaskEntity dbTask = taskService.findOne(task.getId());
		dbTask.setCategory(task.getCategory());
		dbTask.setDate(task.getDate());
		dbTask.setTask(task.getTask());
		dbTask.setTaskPriority(task.getTaskPriority());
		dbTask.setTitle(task.getTitle());
	}

}
