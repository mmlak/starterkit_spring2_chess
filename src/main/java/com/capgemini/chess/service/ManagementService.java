package com.capgemini.chess.service;

import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.exception.InvalidMatchFormatException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.to.MatchTO;

/**
 * Service for managing user ranking and their points.
 * 
 * @author MIMLAK
 *
 */
public interface ManagementService extends Service<MatchBO, Integer> {

	/**
	 * Updates points on players' profiles
	 */
	public void updatePlayerPoints();

	/**
	 * Updates player profiles with matches they have played.
	 * 
	 * @throws UserNotFoundException
	 * @throws InvalidUserException
	 */
	public void updatePlayersMatchHistory() throws UserNotFoundException;

	/**
	 * Ends matches due to too long idle time.
	 * 
	 * @throws InvalidMatchFormatException
	 */
	public int arbitrarlyEndMatchesWithTooLongNoAction() throws InvalidMatchFormatException;

	public void addNewMatch(MatchTO match);
}
