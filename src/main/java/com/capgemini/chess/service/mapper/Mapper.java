package com.capgemini.chess.service.mapper;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserTO;

@Transactional
public class Mapper {

	public static MatchTO map(MatchBO match) {
		UserTO winner = Mapper.map(match.getWinner());
		UserTO loser = Mapper.map(match.getLoser());
		MatchTO matchTO = new MatchTO.MatchTOBuilder().setWinner(winner).setLoser(loser)
				.setWinnerPoints(match.getWinnerPoints()).setLoserPoints(match.getLoserPoints())
				.setMatchId(match.getId()).setLastMovePlayer(Mapper.map(match.getLastMovePlayer()))
				.setTimeSinceLastMove(match.getTimeSinceLastMove()).setGameState(match.getGameState()).build();
		return matchTO;
	}

	public static MatchBO map(MatchTO match) {
		UserBO winner = Mapper.map(match.getWinner());
		UserBO loser = Mapper.map(match.getLoser());
		MatchBO matchBO = new MatchBO.MatchBOBuilder().setWinner(winner).setLoser(loser)
				.setLoserPoints(match.getLoserPoints()).setWinnerPoints(match.getWinnerPoints())
				.setId(match.getMatchId()).setLastMovePlayer(Mapper.map(match.getLastMovePlayer()))
				.setTimeSinceLastMove(match.getTimeSinceLastMove()).setGameState(match.getGameState()).build();
		return matchBO;

	}

	public static UserTO map(UserBO user) {
		UserTO userTO = new UserTO.UserTOBuilder().setId(user.getId()).setLogin(user.getLogin())
				.setPassword(user.getPassword()).setName(user.getName()).setSurname(user.getSurname())
				.setEmail(user.getEmail()).setAboutMe(user.getAboutMe()).setLifeMotto(user.getLifeMotto())
				.setPoints(user.getPoints()).build();
		return userTO;
	}

	public static UserBO map(UserTO user) {
		UserBO userBO = new UserBO.UserEntityBuilder().setId(user.getId()).setLogin(user.getLogin())
				.setPassword(user.getPassword()).setName(user.getName()).setSurname(user.getSurname())
				.setEmail(user.getEmail()).setAboutMe(user.getAboutMe()).setLifeMotto(user.getLifeMotto())
				.setPoints(user.getPoints()).build();
		return userBO;
	}

	public static List<UserTO> mapUser2TO(List<UserBO> findAll) {
		return findAll.stream().map(Mapper::map).collect(Collectors.toList());
	}

	public static List<UserBO> mapUser2BO(List<UserTO> findAll) {
		return findAll.stream().map(Mapper::map).collect(Collectors.toList());
	}

	public static List<MatchTO> mapMatch2TO(List<MatchBO> findAll) {
		return findAll.stream().map(Mapper::map).collect(Collectors.toList());
	}

	public static List<MatchBO> mapMatch2BO(List<MatchTO> findAll) {
		return findAll.stream().map(Mapper::map).collect(Collectors.toList());
	}
}
