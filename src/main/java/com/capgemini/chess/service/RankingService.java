package com.capgemini.chess.service;

import java.util.List;

import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.to.UserTO;

/**
 * Service responsible for ranking management.
 * 
 * @author MIMLAK
 *
 */
public interface RankingService extends Service<MatchBO, Integer> {
	/**
	 * Returns points for specific player.
	 * 
	 * @param id
	 *            user's id
	 * @return points for player
	 * @throws UserNotFoundException
	 */
	public int getPointsForUser(int id) throws UserNotFoundException;

	/**
	 * Returns user position in ranking.
	 * 
	 * @param user
	 *            userBO
	 * @return position in ranking
	 */
	public int getUserPositionInRanking(UserTO user);

	/**
	 * Returns whole ranking
	 * 
	 * @return ranking Set
	 */
	public List<UserBO> getRanking();

}
