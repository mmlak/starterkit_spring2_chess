package com.capgemini.chess.service;

import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.UserNotFoundException;

public interface UserService extends Service<UserBO, Integer> {

	public UserBO readUser(int id) throws UserNotFoundException;

	void updateUser(UserBO user);

}
