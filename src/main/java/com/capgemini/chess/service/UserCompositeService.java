package com.capgemini.chess.service;

import java.util.List;

import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.EntityNotFoundException;
import com.capgemini.chess.exception.InvalidInputDataException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.to.UserTO;

/**
 * User interface for updating self profile and viewing ranking
 * 
 * @author MIMLAK
 *
 */
public interface UserCompositeService extends Service<UserBO, Integer> {

	/**
	 * Views ranking.
	 * 
	 * @return ranking
	 */
	public List<UserTO> getRanking();

	/**
	 * Shows profile of user with given id
	 * 
	 * @param userId
	 *            id of user, whose profile should be shown
	 * @return user profile
	 * @throws UserNotFoundException
	 */
	public UserTO showUserProfile(int userId) throws UserNotFoundException;

	/**
	 * Updates profile
	 * 
	 * @param userProfile
	 *            this user profile
	 * @throws EntityNotFoundException 
	 */
	public void updateProfile(UserTO userProfile) throws EntityNotFoundException;

	public void addUser(UserTO user) throws InvalidInputDataException;

	/**
	 * Returns points for selected user
	 * 
	 * @param id
	 *            user's id
	 * @return
	 * @throws UserNotFoundException
	 */

}
