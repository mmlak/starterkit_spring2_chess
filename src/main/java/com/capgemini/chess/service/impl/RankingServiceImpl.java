package com.capgemini.chess.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.UserTO;

class PointsComparator implements Comparator<UserBO> {

	@Override
	public int compare(UserBO user1, UserBO user2) {
		if (user1.getPoints() > user2.getPoints()) {
			return -1;
		} else if (user2.getPoints() > user1.getPoints()) {
			return 1;
		} else {
			return 0;
		}
	}

}

@Service
@Transactional
class RankingServiceImpl extends AbstractService<MatchBO, Integer> implements RankingService {
	@Autowired
	UserDao userDao;

	@Override
	public int getPointsForUser(int id) throws UserNotFoundException {
		return userDao.findOne(id).getPoints();
	}

	@Override
	public int getUserPositionInRanking(UserTO user) {
		List<UserBO> ranking = getRanking();
		UserBO userEntity = Mapper.map(user);
		return ranking.indexOf(userEntity) + 1;
	}

	@Override
	public List<UserBO> getRanking() {
		List<UserBO> ranking = userDao.findAll();
		Collections.sort(ranking, new PointsComparator());
		return ranking;
	}

}
