package com.capgemini.chess.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.UserService;

@Service
@Transactional
public class UserServiceImpl extends AbstractService<UserBO,Integer> implements UserService {
	@Autowired
	UserDao ud;

	@Override
	public UserBO readUser(int id) throws UserNotFoundException {
		UserBO readUser = ud.findOne(id);
		if (readUser == null) {
			throw new UserNotFoundException();
		} else {
			return ud.findOne(id);
		}
	}

	@Override
	public void updateUser(UserBO user) {
		ud.update(user);
	}
}
