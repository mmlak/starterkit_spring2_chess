package com.capgemini.chess.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.exception.EntityNotFoundException;
import com.capgemini.chess.exception.InvalidInputDataException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.RankingService;
import com.capgemini.chess.service.UserCompositeService;
import com.capgemini.chess.service.UserService;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.UserTO;

@Service
@Transactional
public class PlayerCompositeServiceImpl extends com.capgemini.chess.service.impl.AbstractService<UserBO, Integer>
		implements UserCompositeService {
	@Autowired
	RankingService rankService;
	@Autowired
	UserService userService;
	@Autowired
	UserDao userDao;

	@Override
	public List<UserTO> getRanking() {
		return Mapper.mapUser2TO(rankService.getRanking());
	}

	@Override
	public UserTO showUserProfile(int userId) throws UserNotFoundException {
		return Mapper.map(userService.readUser(userId));
	}

	@Override
	public void updateProfile(UserTO userProfile) throws EntityNotFoundException {
		findOne(userProfile.getId());
		userService.updateUser(Mapper.map(userProfile));
	}

	@Override
	public void addUser(UserTO user) throws InvalidInputDataException {
		try{
		userDao.save(Mapper.map(user));
		}
		catch(DataIntegrityViolationException e){
			throw new InvalidInputDataException(user);
		}
	}

}
