package com.capgemini.chess.service.impl;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dao.MatchDao;
import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.dataaccess.entities.MatchBO;
import com.capgemini.chess.dataaccess.entities.UserBO;
import com.capgemini.chess.enums.GameState;
import com.capgemini.chess.exception.InvalidMatchFormatException;
import com.capgemini.chess.exception.UserNotFoundException;
import com.capgemini.chess.service.ManagementService;
import com.capgemini.chess.service.mapper.Mapper;
import com.capgemini.chess.service.to.MatchTO;
import com.capgemini.chess.service.to.UserTO;

@Service
@Transactional
public class ManagementServiceImpl extends AbstractService<MatchBO, Integer> implements ManagementService {

	@Autowired
	UserDao userDao;
	@Autowired
	MatchDao matchDao;

	@Override
	public void updatePlayerPoints() {
		for (UserBO user : userDao.findAll()) {
			for (MatchBO match : matchDao.getAllMatchesForUser(user.getId())) {
				if (match.getWinner().equals(user) && match.getGameState() != GameState.NOT_FINISHED) {
					user.addPoints(match.getWinnerPoints());
				} else {
					user.addPoints(match.getLoserPoints());
				}
			}
		}
	}

	@Override
	public void updatePlayersMatchHistory() throws UserNotFoundException {
		for (MatchBO match : matchDao.findAll()) {
			UserBO winner = match.getWinner();
			UserBO loser = match.getLoser();
			Set<MatchBO> winnerSet = winner.getMatchSet();
			Set<MatchBO> loserSet = loser.getMatchSet();
			winnerSet.add(match);
			loserSet.add(match);
			winner.setMatchSet(winnerSet);
			loser.setMatchSet(loserSet);

		}
	}

	@Override
	public int arbitrarlyEndMatchesWithTooLongNoAction() throws InvalidMatchFormatException {
		final int MAX_MATCH_IDLE_TIME = 60000;
		Set<MatchTO> workingSet = new HashSet<>();
		int counter = 0;
		for (MatchTO match : Mapper.mapMatch2TO(matchDao.findAll())) {
			if (counter == 2)
				break;
			if (match.getTimeSinceLastMove() > MAX_MATCH_IDLE_TIME && match.getGameState() == GameState.NOT_FINISHED) {
				workingSet.add(match);
				counter++;
			}
		}
		for (MatchTO match : workingSet) {
			UserTO playerOne = match.getWinner();
			UserTO playerTwo = match.getLoser();
			if (match.getLastMovePlayer().equals(playerOne)) {
				MatchTO newMatch = new MatchTO.MatchTOBuilder().setLastMovePlayer(playerOne).setLoser(playerOne)
						.setWinner(playerTwo).setGameState(GameState.FINISHED).setMatchId(match.getMatchId()).build();
				matchDao.update(Mapper.map(newMatch));
			} else {
				MatchTO newMatch = new MatchTO.MatchTOBuilder().setLastMovePlayer(playerOne).setLoser(playerTwo)
						.setWinner(playerOne).setGameState(GameState.FINISHED).setMatchId(match.getMatchId()).build();
				matchDao.update(Mapper.map(newMatch));
			}
		}
		return workingSet.size();
	}

	@Override
	public void addNewMatch(MatchTO match) {
		MatchBO matchEntity = Mapper.map(match);
		matchEntity.setWinner(userDao.findOne(match.getWinner().getId()));
		matchEntity.setLoser(userDao.findOne(match.getLoser().getId()));
		matchEntity.setLastMovePlayer(userDao.findOne(match.getLastMovePlayer().getId()));
		matchDao.save(matchEntity);
	}
}
