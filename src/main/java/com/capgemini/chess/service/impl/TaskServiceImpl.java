package com.capgemini.chess.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dao.TaskDao;
import com.capgemini.chess.dataaccess.entities.TaskEntity;
import com.capgemini.chess.service.TaskService;

@Service
@Transactional
public class TaskServiceImpl extends AbstractService<TaskEntity,Integer> implements TaskService {
	
	@Autowired
	TaskDao taskDao;
	
	@Override
	public void delete(Integer id){
		taskDao.delete(id);
	}

}
