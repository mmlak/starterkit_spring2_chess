package com.capgemini.chess.service.to;

import com.capgemini.chess.enums.GameState;

public class MatchTO {
	private UserTO winner;
	private UserTO loser;
	private int winnerPoints;
	private int loserPoints;
	private int matchId;
	private long timeSinceLastMove;
	private UserTO lastMovePlayer;
	private GameState gameState;

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public long getTimeSinceLastMove() {
		return timeSinceLastMove;
	}

	public void setTimeSinceLastMove(long timeSinceLastMove) {
		this.timeSinceLastMove = timeSinceLastMove;
	}

	public UserTO getLastMovePlayer() {
		return lastMovePlayer;
	}

	public void setLastMovePlayer(UserTO lastMovePlayer) {
		this.lastMovePlayer = lastMovePlayer;
	}

	public UserTO getWinner() {
		return winner;
	}

	public void setWinner(UserTO winner) {
		this.winner = winner;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (matchId ^ (matchId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchTO other = (MatchTO) obj;
		if (matchId != other.matchId)
			return false;
		return true;
	}

	public UserTO getLoser() {
		return loser;
	}

	public void setLoser(UserTO loser) {
		this.loser = loser;
	}

	public int getWinnerPoints() {
		return winnerPoints;
	}

	public void setWinnerPoints(int winnerPoints) {
		this.winnerPoints = winnerPoints;
	}

	public int getLoserPoints() {
		return loserPoints;
	}

	public void setLoserPoints(int loserPoints) {
		this.loserPoints = loserPoints;
	}

	public MatchTO() {

	}

	public MatchTO(MatchTOBuilder builder) {
		this.winner = builder.winner;
		this.loser = builder.loser;
		this.winnerPoints = builder.winnerPoints;
		this.loserPoints = builder.loserPoints;
		this.matchId = builder.matchId;
		this.timeSinceLastMove = builder.timeSinceLastMove;
		this.lastMovePlayer = builder.lastMovePlayer;
		this.gameState = builder.gameState;

	}

	public int getMatchId() {
		return matchId;
	}

	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}

	public static class MatchTOBuilder {
		private GameState gameState;
		private UserTO lastMovePlayer;
		private long timeSinceLastMove;
		private UserTO winner;
		private UserTO loser;
		private int winnerPoints;
		private int loserPoints;
		private int matchId;

		public MatchTOBuilder setGameState(GameState state) {
			this.gameState = state;
			return this;
		}

		public MatchTOBuilder setLastMovePlayer(UserTO user) {
			this.lastMovePlayer = user;
			return this;
		}

		public MatchTOBuilder setTimeSinceLastMove(long time) {
			this.timeSinceLastMove = time;
			return this;
		}

		public MatchTOBuilder setWinner(UserTO winner) {
			this.winner = winner;
			return this;
		}

		public MatchTOBuilder setLoser(UserTO loser) {
			this.loser = loser;
			return this;
		}

		public MatchTOBuilder setWinnerPoints(int points) {
			this.winnerPoints = points;
			return this;
		}

		public MatchTOBuilder setLoserPoints(int points) {
			this.loserPoints = points;
			return this;
		}

		public MatchTO build() {
			return new MatchTO(this);
		}

		public MatchTOBuilder setMatchId(int matchId) {
			this.matchId = matchId;
			return this;
		}
	}
}
