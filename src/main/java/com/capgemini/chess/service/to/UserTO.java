package com.capgemini.chess.service.to;

import java.util.HashSet;
import java.util.Set;

public class UserTO {

	private int id;
	private String login;
	private String password;
	private String name;
	private String surname;
	private String email;

	public UserTO(int id, String login, String password) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
	}

	private String aboutMe;
	private String lifeMotto;
	private int points;
	private Set<MatchTO> matchSet = new HashSet<>();

	public void addPoints(int points) {
		this.points += points;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserTO other = (UserTO) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getLifeMotto() {
		return lifeMotto;
	}

	public void setLifeMotto(String lifeMotto) {
		this.lifeMotto = lifeMotto;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Set<MatchTO> getMatchSet() {
		return matchSet;
	}

	public void setMatchSet(Set<MatchTO> matchSet) {
		this.matchSet = matchSet;
	}

	public UserTO(UserTOBuilder builder) {
		this.id = builder.id;
		this.login = builder.login;
		this.password = builder.password;
		this.name = builder.name;
		this.surname = builder.surname;
		this.email = builder.email;
		this.aboutMe = builder.aboutMe;
		this.lifeMotto = builder.lifeMotto;
		this.points = builder.points;
		this.matchSet = builder.matchSet;

	}

	public UserTO() {

	}

	public static class UserTOBuilder {
		private int id;
		private String login;
		private String password;
		private String name;
		private String surname;
		private String email;
		private String aboutMe;
		private String lifeMotto;
		private int points;
		private Set<MatchTO> matchSet = new HashSet<>();

		public UserTOBuilder setId(int id) {
			this.id = id;
			return this;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserTOBuilder other = (UserTOBuilder) obj;
			if (id != other.id)
				return false;
			return true;
		}

		public UserTOBuilder setLogin(String login) {
			this.login = login;
			return this;
		}

		public UserTOBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public UserTOBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public UserTOBuilder setSurname(String surname) {
			this.surname = surname;
			return this;
		}

		public UserTOBuilder setEmail(String email) {
			this.email = email;
			return this;
		}

		public UserTOBuilder setAboutMe(String aboutMe) {
			this.aboutMe = aboutMe;
			return this;
		}

		public UserTOBuilder setLifeMotto(String lifeMotto) {
			this.lifeMotto = lifeMotto;
			return this;
		}

		public UserTOBuilder setPoints(int points) {
			this.points = points;
			return this;
		}

		public UserTOBuilder setMatchSet(Set<MatchTO> matchSet) {
			this.matchSet = matchSet;
			return this;
		}

		public UserTO build() {
			return new UserTO(this);
		}

	}

}
