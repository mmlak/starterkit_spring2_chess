package com.capgemini.chess.service.to;

import java.util.Date;

import com.capgemini.chess.enums.TaskCategory;

public class TaskTO {
	
		
		private long id;
		private String title;
		private TaskCategory category;
		private String task;
		private Date date;
		
		
		public TaskTO(){
			
		}


		public long getId() {
			return id;
		}


		public void setId(long id) {
			this.id = id;
		}


		public String getTitle() {
			return title;
		}


		public void setTitle(String title) {
			this.title = title;
		}


		public TaskCategory getCategory() {
			return category;
		}


		public void setCategory(TaskCategory category) {
			this.category = category;
		}


		public String getTask() {
			return task;
		}


		public void setTask(String task) {
			this.task = task;
		}


		public Date getDate() {
			return date;
		}


		public void setDate(Date date) {
			this.date = date;
		}



		

	}


