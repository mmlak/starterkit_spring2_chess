package com.capgemini.chess.service;

import com.capgemini.chess.dataaccess.entities.TaskEntity;


public interface TaskService extends com.capgemini.chess.service.Service<TaskEntity, Integer> {

}
